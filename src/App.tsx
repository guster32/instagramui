import React from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import MainScreen from './components/MainScreen';

const AppStackNavigator = StackNavigator({
    Main: {
        screen: MainScreen
    }
});

export interface Props { }
export interface State { }
export default class App extends React.Component<Props, State> {
    render() {
        return (
            <AppStackNavigator/>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
