import React from 'react';
import {
    Card,
    CardItem,
    Thumbnail,
    Body,
    Left,
    Right,
    Button,
    Icon,
    Text
} from 'native-base';
import {
    View,
    StyleSheet,
    Image
} from 'react-native';

export interface Props {
    imageSource: string;
    likes: string;
}
export interface State { }

interface IImages {
    [key: string]: any;
}

const images: IImages = {
    '1': require('../../assets/feed_images/1.jpg'),
    '2': require('../../assets/feed_images/2.jpg'),
    '3': require('../../assets/feed_images/3.jpg'),
};

class CardComponent extends React.Component<Props, State> {

    render() {
        return (
            <Card>
                <CardItem>
                    <Left>
                        <Thumbnail source={ require ('../../assets/me.png')}/>
                        <Body>
                            <Text>TomBates</Text>
                            <Text note>May 27, 2018</Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem cardBody>
                    <Image source={ images[this.props.imageSource]} style={{ height: 200, width: undefined, flex: 1}} />
                </CardItem>
                <CardItem style={{ height: 45 }}>
                    <Left>
                        <Button transparent>
                            <Icon name='ios-heart-outline' style={{color: 'black'}} />
                        </Button>
                        <Button transparent>
                            <Icon name='ios-chatbubbles-outline' style={{color: 'black'}} />
                        </Button>
                        <Button transparent>
                            <Icon name='ios-send-outline' style={{color: 'black'}} />
                        </Button>
                    </Left>
                </CardItem>
                <CardItem style={{height: 35}}>
                    <Text>{ this.props.likes} likes</Text>
                </CardItem>
                <CardItem>
                    <Body>
                        <Text>
                            <Text style={{ fontWeight: '900' }}>TomBates </Text>With Solo, the distaste runs deeper. Star Wars fans object to this movie on principle. Much of the controversy around The Last Jedi revolved around Rian Johnson’s willingness to kill his darlings and make bold alterations to the lore of the series. In a similar way, fans consider Ford’s portrayal of the roguish outlaw sacrosanct. To trade in Ford’s craggy visage for a younger model is the equivalent of giving the Millennium Falcon a trendy new paint job and an obtrusive rear spoiler.
                        </Text>
                    </Body>
                </CardItem>
            </Card>
        );
    }
}

export default CardComponent;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});