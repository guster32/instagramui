import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Platform,
    StyleProp,
    ViewStyle
} from 'react-native';
import {
    Icon,
    Container,
    Content,
    Thumbnail,
    Header,
    Left,
    Right,
    Body
} from 'native-base';
import CardComponent from '../CardComponent';

class HomeTab extends React.Component {
    static navigationOptions = {
        tabBarIcon: ({tintColor}: any) => (
            <Icon name='ios-home' style={{ color: tintColor }}/>
        )
    };

    render() {
        return(
            <Container style={styles.container}>
                <Header style={{backgroundColor: 'white', }}>
                    <Left><Icon name='ios-camera-outline' style={{ paddingLeft: 10}}/></Left>
                    <Body style={ headerBody }><Text>Instagram</Text></Body>
                    <Right><Icon name='ios-send-outline' style={{ paddingRight: 10}}/></Right>
                </Header>
                <Content>
                    <View style={{ height: 100}}>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 7 }}>
                            <Text style={{ fontWeight: 'bold' }}>Stories</Text>
                            <View style={{ flexDirection: 'row', 'alignItems': 'center' }}>
                                <Icon name='md-play' style={{ fontSize: 14}}></Icon>
                                <Text style={{ fontWeight: 'bold' }}>Watch all</Text>
                            </View>
                        </View>
                        <View style={{ flex: 3}}>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                contentContainerStyle={{
                                    alignItems: 'center',
                                    paddingStart: 5,
                                    paddingEnd: 5
                                }}
                            >
                                <Thumbnail
                                style={{ marginHorizontal: 5, borderColor: 'lightblue', borderWidth: 2}}
                                source={ require ('../../../assets/stories_thumbnails/1.jpg')} />
                                <Thumbnail
                                style={{ marginHorizontal: 5, borderColor: 'lightblue', borderWidth: 2}}
                                source={ require ('../../../assets/stories_thumbnails/2.jpg')} />
                                <Thumbnail
                                style={{ marginHorizontal: 5, borderColor: 'lightblue', borderWidth: 2}}
                                source={ require ('../../../assets/stories_thumbnails/3.jpg')} />
                                <Thumbnail
                                style={{ marginHorizontal: 5, borderColor: 'lightblue', borderWidth: 2}}
                                source={ require ('../../../assets/stories_thumbnails/4.jpg')} />
                                <Thumbnail
                                style={{ marginHorizontal: 5, borderColor: 'lightblue', borderWidth: 2}}
                                source={ require ('../../../assets/stories_thumbnails/5.jpg')} />
                                <Thumbnail
                                style={{ marginHorizontal: 5, borderColor: 'lightblue', borderWidth: 2}}
                                source={ require ('../../../assets/stories_thumbnails/6.jpg')} />
                                <Thumbnail
                                style={{ marginHorizontal: 5, borderColor: 'lightblue', borderWidth: 2}}
                                source={ require ('../../../assets/stories_thumbnails/7.jpg')} />
                                <Thumbnail
                                style={{ marginHorizontal: 5, borderColor: 'lightblue', borderWidth: 2}}
                                source={ require ('../../../assets/stories_thumbnails/8.jpg')} />
                            </ScrollView>
                        </View>
                    </View>
                    <CardComponent imageSource='1' likes='94'/>
                    <CardComponent imageSource='2' likes='27'/>
                    <CardComponent imageSource='3' likes='35'/>
                </Content>
            </Container>
        );
    }
}



export default HomeTab;

// See: https://github.com/GeekyAnts/NativeBase/issues/82
const headerBody: any = {
    ...Platform.select({android: {
        backgroundColor: 'white',
        flex: 1,
        alignItems: 'flex-end'
    }})
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    }
});