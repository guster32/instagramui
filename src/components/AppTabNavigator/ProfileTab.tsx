import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Platform,
    Image,
    Dimensions
} from 'react-native';
import {
    Icon,
    Container,
    Content,
    Header,
    Left,
    Body,
    Right,
    Button
} from 'native-base';

import EntypoIcon from 'react-native-vector-icons/Entypo';
import CardComponent from '../CardComponent';

const {width, height} = Dimensions.get('window');
const images = [
    require('../../../assets/feed_images/1.jpg'),
    require('../../../assets/feed_images/2.jpg'),
    require('../../../assets/feed_images/3.jpg'),
    require('../../../assets/feed_images/4.jpg'),
    require('../../../assets/feed_images/5.jpg'),
    require('../../../assets/feed_images/6.jpg'),
    require('../../../assets/feed_images/7.jpg'),
    require('../../../assets/feed_images/8.jpg'),
    require('../../../assets/feed_images/9.jpg'),
    require('../../../assets/feed_images/10.jpg'),
    require('../../../assets/feed_images/11.jpg'),
    require('../../../assets/feed_images/12.jpg')
];


export interface IProfileProps {}

export interface IProfileState {
    activeIndex: number;
}

class ProfileTab extends React.Component<IProfileProps, IProfileState> {
    static navigationOptions = {
        tabBarIcon: ({tintColor}: any) => (
            <Icon name='ios-person' style={{ color: tintColor }}/>
        )
    };

    constructor(props: IProfileProps) {
        super(props);
        this.state = {
            activeIndex: 0
        };
    }

    segmentClicked = (index: number) => {
        this.setState({activeIndex: index});
    }

    renderSectionOne = () => {
        return images.map((image, index) => {
            return (
                <View key={index} style={[
                        {width: (width) / 3},
                        {height: (width / 3)},
                        index % 3 !== 0 ? { paddingLeft: 2} : { paddingLeft: 0 },
                        {marginBottom: 2}
                        ]}>
                    <Image source={image}
                        style={{ flex: 1, width: undefined, height: undefined }}/>
                </View>
            );
        });
    }

    renderSection = () => {
        if (this.state.activeIndex === 0) {
            return (
                <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                    { this.renderSectionOne() }
                </View>
            );
        } else if (this.state.activeIndex === 1) {
            return(
                <View>
                    <CardComponent imageSource='1' likes='100'/>
                    <CardComponent imageSource='2' likes='100'/>
                    <CardComponent imageSource='3' likes='100'/>
                </View>
            );
        }
    }

    render() {
        return(
            <Container style={{ flex: 1, backgroundColor: 'white'}}>
                <Header style={{backgroundColor: 'white', }}>
                    <Left><Icon name='md-person-add' style={{ paddingLeft: 10}}/></Left>
                    <Body style={ headerBody }><Text>TomBates</Text></Body>
                    <Right><EntypoIcon name='back-in-time' style={{ paddingRight: 10, fontSize: 32 }}/></Right>
                </Header>
                <Content>
                    <View style={{ paddingTop: 10}}>
                        <View style={{flexDirection: 'row'}}>
                            <View style={{ flex: 1, alignItems: 'center'}}>
                                <Image source={require('../../../assets/me.png')} style={{ width: 75, height: 75, borderRadius: 37.5}} />
                            </View>
                            <View style={{ flex: 3}}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-around'}}>
                                    <View style={{ alignItems: 'center'}}>
                                        <Text>20</Text>
                                        <Text style={{ fontSize: 10, color: 'gray'}}>posts</Text>
                                    </View>
                                    <View style={{ alignItems: 'center'}}>
                                        <Text>26</Text>
                                        <Text style={{ fontSize: 10, color: 'gray'}}>followers</Text>
                                    </View>
                                    <View style={{ alignItems: 'center'}}>
                                        <Text>157</Text>
                                        <Text style={{ fontSize: 10, color: 'gray'}}>following</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', paddingTop: 10}}>
                                    <Button bordered dark style={{flex: 3, marginLeft: 10, justifyContent: 'center', height: 35}}>
                                        <Text>Edit Profiles</Text>
                                    </Button>
                                    <Button bordered dark style={{flex: 1, height: 35, marginRight: 10, marginLeft: 5, justifyContent: 'center'}}>
                                        <Icon name='settings' style={{ fontSize: 20}}/>
                                    </Button>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{paddingVertical: 10, paddingHorizontal: 10}}>
                        <Text style={{fontWeight: 'bold'}}>Tom Bates</Text>
                        <Text>Attorney | Soccer Player | 80's Music buff</Text>
                        <Text>www.unsureprogramer.com</Text>
                    </View>
                    <View>
                        <View style={{flexDirection: 'row', justifyContent: 'space-around', borderTopWidth: 1, borderTopColor: '#eae5e5'}}>
                            <Button transparent
                                onPress={() => this.segmentClicked(0)}
                                active={this.state.activeIndex === 0}
                            >
                                <Icon name='ios-apps-outline'
                                    style={[this.state.activeIndex === 0 ? {} : {color: 'grey'}]}
                                />
                            </Button>
                            <Button transparent onPress={() => this.segmentClicked(1)}
                                active={this.state.activeIndex === 1}
                            >
                                <Icon name='ios-list-outline'
                                    style={[this.state.activeIndex === 1 ? {} : {color: 'grey'}]}
                                />
                            </Button>
                            <Button transparent onPress={() => this.segmentClicked(2)}
                                active={this.state.activeIndex === 2}
                            >
                                <Icon name='ios-people-outline'
                                    style={[this.state.activeIndex === 2 ? {} : {color: 'grey'}]}
                                />
                            </Button>
                            <Button transparent onPress={() => this.segmentClicked(3)}
                                active={this.state.activeIndex === 3}
                            >
                                <Icon name='ios-bookmark-outline'
                                    style={[this.state.activeIndex === 3 ? {} : {color: 'grey'}]}
                                />
                            </Button>
                        </View>
                        {this.renderSection()}
                    </View>
                </Content>
            </Container>
        );
    }
}



export default ProfileTab;
// See: https://github.com/GeekyAnts/NativeBase/issues/82
const headerBody: any = {
    ...Platform.select({android: {
        backgroundColor: 'white',
        alignItems: 'flex-end'
    }})
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});