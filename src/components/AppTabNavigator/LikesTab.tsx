import React from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';
import { Icon } from 'native-base';

class LikedTab extends React.Component {
    static navigationOptions = {
        tabBarIcon: ({tintColor}: any) => (
            <Icon name='ios-heart' style={{ color: tintColor }}/>
        )
    };

    render() {
        return(
            <View style={styles.container}>
                <Text>LikedTab</Text>
            </View>
        );
    }
}



export default LikedTab;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});