# Instagramui

![](instagram.png)
Instagram Ui clone. I followed youtube tutorial from [here]((https://www.youtube.com/watch?v=cgg1HidN4mQ)) with the exception that I did not use Expo and I used typescript. This is what the end result looks like:
![](instagramui.gif)